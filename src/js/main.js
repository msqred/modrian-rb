'use strict';

const bulletWrap = document.querySelector('.new_first--hero_bullets');
const firstItemArr = document.querySelectorAll('.new_first--hero_item');
const joinItemArr = document.querySelectorAll('.new_first--hero_join--item');
const secondLeftItemArr = document.querySelectorAll('.new_second--left_item');
const secondRightItemArr = document.querySelectorAll('.new_second--right_item');

const createBullets = (count, wrap) => {
    for (let i = 0; i < count.length; i++) {
        let bullet = document.createElement('i');
        wrap.appendChild(bullet);
    }
}

const deleteActive = (arr) => {
    for (let i = 0; i < arr.length; i++) {
        arr[i].classList.remove('active');
    }
}


createBullets(firstItemArr, bulletWrap);


const bullets = document.querySelectorAll('.new_first--hero_bullets i');

for (let i = 0; i < bullets.length; i++) {
    bullets[i].addEventListener('click', (e) => {
        deleteActive(firstItemArr);
        deleteActive(joinItemArr);
        deleteActive(secondLeftItemArr);
        deleteActive(secondRightItemArr);
        deleteActive(bullets);
        firstItemArr[i].classList.add('active');
        joinItemArr[i].classList.add('active');
        secondLeftItemArr[i].classList.add('active');
        secondRightItemArr[i].classList.add('active');
        bullets[i].classList.add('active');
    })
}

bullets[0].classList.add('active');